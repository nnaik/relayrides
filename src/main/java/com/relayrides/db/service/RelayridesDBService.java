package com.relayrides.db.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.relayrides.db.RelayridesDB;

public class RelayridesDBService {

	/**
	 * storing singleton instance for RelayridesDBService
	 */
	private static volatile RelayridesDBService instance;
	
	/**
	 * storing instance of RelayRides DB
	 */
	private RelayridesDB relayRidesDB;
	
	/**
	 * storing list of newly created transactions
	 */
	private List<RelayridesDB> transactionalDB;
	
	/**
	 * making constructor private to support singleton
	 */
	private RelayridesDBService(){
		relayRidesDB = new RelayridesDB();
		transactionalDB = new LinkedList<RelayridesDB>();
	}
	
	/**
	 * Guarantees the creation of a single instance across the virtual machine.
	 * Assumed to be called very frequently.
	 * 
	 * @return an instance of RelayridesDBService
	 */
	public static RelayridesDBService getInstance(){
		if(instance==null){ // check if null
			synchronized(RelayridesDBService.class){// get lock on class
				if(instance==null){// double check if still null 
					instance = new RelayridesDBService(); // create new instance
				}
			}
		}
		
		return instance;
	}
	
	/**
	 * Creates new transactions for call and add it to list
	 */
	public void beginTransaction(){
		RelayridesDB relayRidesDB = new RelayridesDB();
		transactionalDB.add(relayRidesDB);
	}
	
	/**
	 * Get value from current transaction or from DB
	 * 
	 * @param key
	 * @return value as a string, NULL if value is not set
	 */
	public String get(String key){
		Integer value = null;
		RelayridesDB relayRidesDB=null;
		boolean transactionDbHasKey = false;
		if(transactionalDB.size() > 0){
			for(int i = transactionalDB.size() - 1; i >= 0; --i){
				if((relayRidesDB = transactionalDB.get(i)).containsKey(key)){
					transactionDbHasKey = true;
					break;
				}
			}
		}
		
		if(!transactionDbHasKey)
			relayRidesDB = this.relayRidesDB;
		
		return ((value=relayRidesDB.get(key))==null)? "NULL" : value.toString();
	}
	
	/**
	 * Set value against key in current transaction or in DB
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, Integer value){
		RelayridesDB relayRidesDB=this.relayRidesDB;
		int size = 0;
		if((size = transactionalDB.size()) > 0){
			relayRidesDB = transactionalDB.get(size - 1);
			Set<String> keySet = this.relayRidesDB.getKeySet(value);
			if(keySet!=null)
				relayRidesDB.updateReverseIndex(value, keySet);
		}
		
		relayRidesDB.set(key, value);
	}
	
	/**
	 * Unset entry from DB.
	 * 
	 * @param key
	 */
	public void unset(String key){
		int size = 0;
		boolean foundKey = false;
		Set<String> keySet;
		if((size = transactionalDB.size()) > 0){
			RelayridesDB relayRidesDB = transactionalDB.get(size - 1);
			for(int i=size - 1; i >= 0; --i ){
				if(transactionalDB.get(i).containsKey(key)){
					foundKey = true;
					Integer value = transactionalDB.get(i).get(key);
					if(value!=null){
						keySet = transactionalDB.get(i).getKeySet(value);
						relayRidesDB.updateReverseIndex(value, keySet);
						relayRidesDB.set(key, value);
					}
					
					break;
				}
			}
			
			if(!foundKey){
				Integer value = this.relayRidesDB.get(key);
				keySet = this.relayRidesDB.getKeySet(value);
				relayRidesDB.updateReverseIndex(value, keySet);
				relayRidesDB.set(key, value);
			}
			
			relayRidesDB.softUnset(key);
		} else {
			this.relayRidesDB.unset(key);
		}
		
	}
	
	/**
	 * Rollback current transaction and closes it. 
	 * If no transaction present then prints 'NO TRANSACTION'.
	 * 
	 */
	public void rollbackTransaction(){
		int size = 0;
		if((size=transactionalDB.size()) > 0){
			transactionalDB.remove(size - 1);
		}else {
			System.out.println("NO TRANSACTION");
		}
	}
	
	/**
	 * Commit and close all transactions. 
	 * If no transaction present then prints 'NO TRANSACTION'.
	 */
	public void commit(){
		RelayridesDB relayRidesDB=null;
		if(transactionalDB.size() > 0){	
			while(transactionalDB.size() > 0){
				relayRidesDB = transactionalDB.remove(0);
				for(Entry<String, Integer> entry : relayRidesDB.entrySet()){
					if(entry.getValue()==null)
						this.relayRidesDB.unset(entry.getKey());
					else
						this.relayRidesDB.set(entry.getKey(), entry.getValue());
				}
			}
		}else {
			System.out.println("NO TRANSACTION");
		}
	}
	
	/**
	 * Returns number of key associated with given value
	 * 
	 * @param value
	 * @return count
	 */
	public int keyCountWith(Integer value){
		RelayridesDB relayRidesDB = null;
		boolean transactionDbHasValue = false;
		int size = 0;
		if((size = transactionalDB.size()) > 0){
			for(int i = size - 1; i >= 0; --i){
				if((relayRidesDB = transactionalDB.get(i)).containsValue(value)){
					transactionDbHasValue = true;
					break;
				}
			}
		}
		
		if(!transactionDbHasValue)
			relayRidesDB = this.relayRidesDB;
		
		return relayRidesDB.keyCountWith(value);
	}
	
	/**
	 * Destroy service
	 */
	public void destroy(){
		if(transactionalDB.size() > 0){	
			while(transactionalDB.size() > 0){
				transactionalDB.remove(0);
			}
		}
		transactionalDB = null;
		relayRidesDB = null;
		instance = null;
	}
}
