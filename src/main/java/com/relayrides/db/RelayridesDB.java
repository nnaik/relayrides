package com.relayrides.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public class RelayridesDB {
	/**
	 * Map which stores key and values as a string. This acts as a in memory DB.
	 */
	private Map<String, Integer> relayRidesMapDB;
	private Map<Integer, Set<String>> reverseIndexRelayRidesMapDB;
	
	/**
	 * making constructor private to support singleton
	 */
	public RelayridesDB(){
		relayRidesMapDB = new HashMap<String, Integer>();
		reverseIndexRelayRidesMapDB = new HashMap<Integer, Set<String>>();
	}
	
	/**
	 * Set value for given key. Set reverse index.
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, Integer value){
		unset(key);
		
		relayRidesMapDB.put(key, value);
		Set<String> keySet = reverseIndexRelayRidesMapDB.get(value);
		
		if(keySet==null){
			keySet= new TreeSet<String>();
		}
		keySet.add(key);
		reverseIndexRelayRidesMapDB.put(value, keySet);
	}
	
	/**
	 * Retrieves value for given key
	 * 
	 * @param key
	 * @return value (NULL in case of key not present)
	 */
	public Integer get(String key){
		return relayRidesMapDB.get(key);
	}
	
	/**
	 * Unset value for given key. Unset reverse index.
	 * 
	 * @param key
	 */
	public void softUnset(String key){
		Integer value = relayRidesMapDB.put(key, null);
		if(value!=null){
			Set<String> keySet = reverseIndexRelayRidesMapDB.get(value);
			
			if(keySet!=null){
				keySet.remove(key);
				reverseIndexRelayRidesMapDB.put(value, keySet);
			}else
				reverseIndexRelayRidesMapDB.put(value, new TreeSet<String>());
		}
	}
	
	/**
	 * Unset value for given key. Unset reverse index.
	 * 
	 * @param key
	 */
	public void unset(String key){
		Integer value = relayRidesMapDB.remove(key);
		if(value!=null){
			reverseIndexRelayRidesMapDB.get(value).remove(key);
		}
	}
	
	/**
	 * Give count for key associated with given value
	 * 
	 * @param value
	 * @return count (int value for keys wit give value)
	 */
	public int keyCountWith(Integer value){
		int count = 0;
		
		Set<String> keySet = reverseIndexRelayRidesMapDB.get(value);
		if(keySet!=null)
			count = keySet.size();
		
		return count;
	}
	
	/**
	 * Updates reverse index by given keySet
	 * @param value
	 * @param keySet
	 */
	public void updateReverseIndex(Integer value, Set<String> keySet ){
		if(reverseIndexRelayRidesMapDB.get(value)!=null){
			reverseIndexRelayRidesMapDB.get(value).clear();
		}else{
			reverseIndexRelayRidesMapDB.put(value, new TreeSet<String>());
		}
		
		reverseIndexRelayRidesMapDB.get(value).addAll(keySet);
	}
	
	/**
	 * Returns keySet for given value
	 * 
	 * @param value
	 * @return keySet
	 */
	public Set<String> getKeySet(Integer value){
		return reverseIndexRelayRidesMapDB.get(value);
	}
	
	/**
	 * Returns true in case key present into DB else returns false
	 * @param key
	 * @return true/false
	 */
	public boolean containsKey(String key){
		return relayRidesMapDB.containsKey(key);
	}
	
	/**
	 * Returns true in case key present into reverse index DB else returns false
	 * @param key
	 * @return true/false
	 */
	public boolean containsValue(Integer value){
		return reverseIndexRelayRidesMapDB.containsKey(value);
	}
	
	/**
	 * Gets all set of entry
	 * 
	 * @return Set of entry
	 */
	public Set<Entry<String, Integer>> entrySet(){
		return relayRidesMapDB.entrySet();
	}
	
}
