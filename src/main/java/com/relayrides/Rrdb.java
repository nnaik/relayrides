package com.relayrides;

import java.util.Scanner;

import com.relayrides.db.service.RelayridesDBService;

public class Rrdb {

	enum Command{
		SET, UNSET, GET, NUMEQUALTO, BEGIN, ROLLBACK, COMMIT, END
	}
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		//get instance 
		final RelayridesDBService rrdbService = RelayridesDBService.getInstance();
		Scanner scanner = new Scanner(System.in);
		boolean run = true;
		
		while(run){
			String key;
			Integer value;
			String command = scanner.next();
			
			try{
				switch(Command.valueOf(command.toUpperCase())){
				case SET: key = scanner.next();
							value = scanner.nextInt();
							rrdbService.set(key, value);
							break;
				case UNSET: key = scanner.next();
							rrdbService.unset(key);
							break;
				case GET: key = scanner.next();
							System.out.println(rrdbService.get(key));
							break;
				case NUMEQUALTO : value = scanner.nextInt();;
							System.out.println(rrdbService.keyCountWith(value));
							break;
				case BEGIN: rrdbService.beginTransaction();
							break;
				case ROLLBACK: rrdbService.rollbackTransaction();
							break;
				case COMMIT: rrdbService.commit();
							break;
				case END: run=false;
							break;
				}
			}catch(IllegalArgumentException e){
				StringBuilder sb = new StringBuilder("only allowed commands are: ");
				for(Command c :Command.values()){
					sb.append(c).append(",");
				}
				System.out.println(sb);
			}
			
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() { 
		       rrdbService.destroy();
		    }
		 });
	}

}
