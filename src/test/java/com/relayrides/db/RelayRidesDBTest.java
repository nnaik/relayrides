package com.relayrides.db;

import org.junit.Assert;
import org.junit.Test;

public class RelayRidesDBTest {

	RelayridesDB rrdb = new RelayridesDB();
	
	@Test
	public void testSetNGet(){
		rrdb.set("a", 5);
		Assert.assertEquals(5,rrdb.get("a").intValue());
		rrdb.set("a", 10);
		Assert.assertEquals(10,rrdb.get("a").intValue());
		rrdb.set("a", null);
		Assert.assertSame(null,rrdb.get("a"));
		Assert.assertSame(null,rrdb.get("c"));
	}
	
	@Test
	public void testUnset(){
		rrdb.set("a", 5);
		Assert.assertEquals(5,rrdb.get("a").intValue());
		rrdb.unset("a");
		Assert.assertSame(null,rrdb.get("a"));
		Assert.assertFalse(rrdb.containsKey("a"));
	}
	
	@Test
	public void testSoftUnset(){
		rrdb.set("a", 5);
		Assert.assertEquals(5,rrdb.get("a").intValue());
		rrdb.softUnset("a");
		Assert.assertSame(null,rrdb.get("a"));
		Assert.assertTrue(rrdb.containsKey("a"));
	}
	
	@Test
	public void testKeyCount(){
		rrdb.set("a", 10);
		rrdb.set("b", 10);
		Assert.assertEquals(2,rrdb.keyCountWith(10));
		rrdb.set("x", 10);
		Assert.assertEquals(3,rrdb.keyCountWith(10));
		rrdb.set("x", 5);
		Assert.assertEquals(2,rrdb.keyCountWith(10));
		rrdb.unset("b");
		Assert.assertEquals(1,rrdb.keyCountWith(10));
		rrdb.set("a", 5);
		Assert.assertEquals(0,rrdb.keyCountWith(10));
	}
	
	@Test
	public void testContainsValue(){
		rrdb.set("a", 10);
		Assert.assertTrue(rrdb.containsValue(10));
		Assert.assertFalse(rrdb.containsValue(30));
	}
	
	@Test
	public void testKeySet(){
		rrdb.set("a", 10);
		rrdb.set("b", 10);
		Assert.assertEquals(2,rrdb.getKeySet(10).size());
		rrdb.set("x", 10);
		Assert.assertEquals(3,rrdb.getKeySet(10).size());
		rrdb.set("x", 5);
		Assert.assertEquals(2,rrdb.getKeySet(10).size());
		rrdb.unset("b");
		Assert.assertEquals(1,rrdb.getKeySet(10).size());
		rrdb.set("a", 5);
		Assert.assertEquals(0,rrdb.getKeySet(10).size());
	}
}
