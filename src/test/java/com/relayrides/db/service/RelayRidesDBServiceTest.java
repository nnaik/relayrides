package com.relayrides.db.service;

import org.junit.Assert;
import org.junit.Test;

public class RelayRidesDBServiceTest {
	RelayridesDBService rrdbs = RelayridesDBService.getInstance();
	
	@Test
	public void testSetNGet(){
		rrdbs.set("a", 5);
		Assert.assertEquals("5",rrdbs.get("a"));
		rrdbs.set("a", 10);
		Assert.assertEquals("10",rrdbs.get("a"));
		rrdbs.set("a", null);
		Assert.assertSame("NULL",rrdbs.get("a"));
		Assert.assertSame("NULL",rrdbs.get("c"));
		rrdbs.destroy();
	}
	
	@Test
	public void testUnset(){
		rrdbs = RelayridesDBService.getInstance();
		rrdbs.set("a", 5);
		Assert.assertEquals("5",rrdbs.get("a"));
		rrdbs.unset("a");
		Assert.assertSame("NULL",rrdbs.get("a"));
		rrdbs.destroy();
	}

	
	@Test
	public void testKeyCount(){
		rrdbs = RelayridesDBService.getInstance();
		rrdbs.set("a", 10);
		rrdbs.set("b", 10);
		Assert.assertEquals(2,rrdbs.keyCountWith(10));
		rrdbs.set("x", 10);
		Assert.assertEquals(3,rrdbs.keyCountWith(10));
		rrdbs.set("x", 5);
		Assert.assertEquals(2,rrdbs.keyCountWith(10));
		rrdbs.unset("b");
		Assert.assertEquals(1,rrdbs.keyCountWith(10));
		rrdbs.set("a", 5);
		Assert.assertEquals(0,rrdbs.keyCountWith(10));
		rrdbs.destroy();
	}
	
	@Test
	public void testRollback(){
		rrdbs = RelayridesDBService.getInstance();
		rrdbs.beginTransaction();
		rrdbs.set("p", 30);
		Assert.assertEquals(1,rrdbs.keyCountWith(30));
		rrdbs.beginTransaction();
		rrdbs.set("p", 20);
		Assert.assertEquals("20",rrdbs.get("p"));
		rrdbs.rollbackTransaction();
		Assert.assertEquals("30",rrdbs.get("p"));
		rrdbs.rollbackTransaction();
		Assert.assertEquals("NULL",rrdbs.get("p"));
		rrdbs.destroy();
	}
	
	@Test
	public void testCommit(){
		rrdbs = RelayridesDBService.getInstance();
		rrdbs.set("p", 50);
		rrdbs.beginTransaction();
		rrdbs.set("p", 30);
		Assert.assertEquals(1,rrdbs.keyCountWith(30));
		rrdbs.beginTransaction();
		rrdbs.set("p", 20);
		Assert.assertEquals("20",rrdbs.get("p"));
		rrdbs.rollbackTransaction();
		Assert.assertEquals("30",rrdbs.get("p"));
		rrdbs.rollbackTransaction();
		Assert.assertEquals("50",rrdbs.get("p"));
		rrdbs.destroy();
	}
	
}
