*****************************************
    Relay Rides In Memory DB
*****************************************
This DB is in memory DB which supports
transactions and provides commandline interface only.


*****************************************
    Compilation Instructions
*****************************************
1. Unzip and explode files into 'relayrides' folder
2. Compile java files with following command
javac -d relayrides/classes relayrides/*.java relayrides/db/*.java relayrides/db/service/*.java
3. Go to classes directory
cd relayrides/classes
4. Create jar file with following command
jar cvfm rrdb.jar Manifest.txt com


******************************************
           Data Commands
******************************************
SET name value – Set the variable name to the value value. Neither variable names nor values will contain spaces.

GET name – Print out the value of the variable name, or NULL if that variable is not set.

UNSET name – Unset the variable name, making it just like that variable was never set.

NUMEQUALTO value – Print out the number of variables that are currently set to value. If no variables equal that value, print 0.

END – Exit the program. Your program will always receive this as its last command.

BEGIN – Open a new transaction block. Transaction blocks can be nested; a BEGIN can be issued inside of an existing block.

ROLLBACK – Undo all of the commands issued in the most recent transaction block, and close the block. Print nothing if successful, or print NO TRANSACTION if no transaction is in progress.

COMMIT – Close all open transaction blocks, permanently applying the changes made in them. Print nothing if successful, or print NO TRANSACTION if no transaction is in progress.
******************************************
            OUTPUT
******************************************
$ java -jar rrdb.jar < test_input/inp1.txt 
10
20
10
NULL
$ java -jar rrdb.jar < test_input/inp2.txt 
10
NULL
$ java -jar rrdb.jar < test_input/inp3.txt 
2
0
1
$ java -jar rrdb.jar < test_input/inp4.txt 
10
20
10
NULL
$ java -jar rrdb.jar < test_input/inp5.txt 
40
NO TRANSACTION
$ java -jar rrdb.jar < test_input/inp6.txt 
50
NULL
60
60
$ java -jar rrdb.jar < test_input/inp7.txt 
1
0
1

